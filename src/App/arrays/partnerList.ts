import mria from '../../img/sliderOne/mria.png';
import gazprom from '../../img/sliderOne/gazprom.png';
import climatec from '../../img/sliderOne/climatec.png';
import britanicaProject from '../../img/sliderOne/britanicaProject.png';
import lesResourse from '../../img/sliderOne/lesResourse.png';
import pivDvor from '../../img/sliderOne/pivDvor.png';
import rosterhit from '../../img/sliderOne/rosterhit.png';
import scanLite from '../../img/sliderOne/scanLite.png';
import oneC from '../../img/sliderOne/1c.png';
import sap from '../../img/sliderOne/sap.png';
import kaidzen from '../../img/sliderOne/kaidzen.png';
import statusFinance from '../../img/sliderOne/StatusFinance.png';

const partnerList = [{
                name: 'mria',
                src: mria
        },
        {
                name: 'gazprom',
                src: gazprom
        },
        {
                name: 'climatec',
                src: climatec
        },
        {
                name: 'britanicaProject',
                src: britanicaProject
        },
        {
                name: 'lesResourse',
                src: lesResourse
        },
        {
                name: 'pivDvor',
                src: pivDvor
        },
        {
                name: 'rosterhit',
                src: rosterhit
        },
        {
                name: 'scanLite',
                src: scanLite
        },
        {
                name: 'oneC',
                src: oneC
        },
        {
                name: 'sap',
                src: sap
        },
        {
                name: 'kaidzen',
                src: kaidzen
        },
        {
                name: 'statusFinance',
                src: statusFinance
        }
];

export default partnerList;
